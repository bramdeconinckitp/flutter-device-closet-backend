class AppConfig {
  // General
  static const String baseUrl = 'testdevices.itp';
  static const String jsonContentType = 'application/json';
  static const bool trustSelfSignedCertificate = true;

  // Devices
  static const String devicesEndpoint = '/api/devices';
  static const String deviceIdParam = 'device_id';

  // Users
  static const String usersEndpoint = '/api/users';
}
