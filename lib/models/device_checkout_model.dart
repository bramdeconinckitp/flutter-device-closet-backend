import 'package:device_closet_backend/device_closet_backend.dart';

class DeviceCheckoutModel extends Serializable {
  static const String _userIdField = 'userId';

  int userId;

  @override
  Map<String, Object> asMap() {
    return {
      _userIdField: userId,
    };
  }

  @override
  void readFromMap(Map<String, Object> object) {
    userId = object[_userIdField] as int;
  }
}
