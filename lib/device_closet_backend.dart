/// device_closet_backend
///
/// An Aqueduct web server.
library device_closet_backend;

export 'dart:async';
export 'dart:io';

export 'package:aqueduct/aqueduct.dart';

export 'channel.dart';
