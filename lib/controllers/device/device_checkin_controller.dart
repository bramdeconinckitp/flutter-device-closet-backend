import 'dart:convert';

import 'package:device_closet_backend/constants/app_config.dart';
import 'package:device_closet_backend/device_closet_backend.dart';
import 'package:http/io_client.dart';

class DeviceCheckinController extends ResourceController {
  static final IOClient _ioClient = IOClient(HttpClient()
    ..badCertificateCallback = (X509Certificate cert, String host, int port) => AppConfig.trustSelfSignedCertificate);

  @Operation.post(AppConfig.deviceIdParam)
  Future<Response> checkDeviceIn(@Bind.path(AppConfig.deviceIdParam) int deviceId) async {
    final uri = Uri.https(AppConfig.baseUrl, '${AppConfig.devicesEndpoint}/$deviceId/checkin');
    final res = await _ioClient.post(uri);

    return Response.ok(json.decode(res.body));
  }
}
