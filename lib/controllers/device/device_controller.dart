import 'dart:convert';

import 'package:device_closet_backend/constants/app_config.dart';
import 'package:device_closet_backend/device_closet_backend.dart';
import 'package:http/io_client.dart';

class DeviceController extends ResourceController {
  static final IOClient _ioClient = IOClient(HttpClient()
    ..badCertificateCallback = (X509Certificate cert, String host, int port) => AppConfig.trustSelfSignedCertificate);

  @Operation.get()
  Future<Response> getAllDevices() async {
    final uri = Uri.https(AppConfig.baseUrl, AppConfig.devicesEndpoint);
    final res = await _ioClient.get(uri);

    if (res.statusCode == HttpStatus.ok) {
      return Response.ok(json.decode(res.body));
    }

    return Response.badRequest(body: json.decode(res.body));
  }

  @Operation.get(AppConfig.deviceIdParam)
  Future<Response> getDeviceById(
    @Bind.path(AppConfig.deviceIdParam) int deviceId,
  ) async {
    final uri = Uri.https(AppConfig.baseUrl, '${AppConfig.devicesEndpoint}/$deviceId');
    final res = await _ioClient.get(uri);

    if (res.statusCode == HttpStatus.ok) {
      return Response.ok(json.decode(res.body));
    }

    return Response.badRequest(body: json.decode(res.body));
  }
}
