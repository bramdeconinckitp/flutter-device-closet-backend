import 'dart:convert';

import 'package:device_closet_backend/constants/app_config.dart';
import 'package:device_closet_backend/device_closet_backend.dart';
import 'package:device_closet_backend/models/device_checkout_model.dart';
import 'package:http/io_client.dart';

class DeviceCheckoutController extends ResourceController {
  static final IOClient _ioClient = IOClient(HttpClient()
    ..badCertificateCallback = (X509Certificate cert, String host, int port) => AppConfig.trustSelfSignedCertificate);

  static const Map<String, String> _headers = {
    HttpHeaders.contentTypeHeader: AppConfig.jsonContentType,
  };

  @Operation.post(AppConfig.deviceIdParam)
  Future<Response> checkDeviceOut(
    @Bind.path(AppConfig.deviceIdParam) int deviceId,
    @Bind.body() DeviceCheckoutModel body,
  ) async {
    final uri = Uri.https(AppConfig.baseUrl, '${AppConfig.devicesEndpoint}/$deviceId/checkout');
    final res = await _ioClient.post(uri, headers: _headers, body: json.encode(body.asMap()));

    return Response.ok(json.decode(res.body));
  }
}
