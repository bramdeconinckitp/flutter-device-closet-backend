import 'package:device_closet_backend/constants/app_config.dart';
import 'package:device_closet_backend/controllers/device/device_checkin_controller.dart';
import 'package:device_closet_backend/controllers/device/device_checkout_controller.dart';
import 'package:device_closet_backend/controllers/device/device_controller.dart';
import 'package:device_closet_backend/controllers/user/user_controller.dart';

import 'device_closet_backend.dart';

class DeviceClosetBackendChannel extends ApplicationChannel {
  @override
  Future<void> prepare() async {
    logger.onRecord.listen((rec) => print('$rec ${rec.error ?? ''} ${rec.stackTrace ?? ''}'));
  }

  @override
  Controller get entryPoint {
    final router = Router();

    router.route(AppConfig.usersEndpoint).link(() => UserController());

    router.route('${AppConfig.devicesEndpoint}/[:${AppConfig.deviceIdParam}]').link(() => DeviceController());
    router.route('${AppConfig.devicesEndpoint}/:${AppConfig.deviceIdParam}/checkin').link(() => DeviceCheckinController());
    router.route('${AppConfig.devicesEndpoint}/:${AppConfig.deviceIdParam}/checkout').link(() => DeviceCheckoutController());

    return router;
  }
}
